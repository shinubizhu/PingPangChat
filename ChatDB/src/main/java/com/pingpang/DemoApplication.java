package com.pingpang;

import java.util.Properties;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@EnableDubbo
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	 //DatabaseIdProvider元素主要是为了支持不同的数据库
	@Bean
	public DatabaseIdProvider getDatabaseIdProvider() {
		DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
		Properties properties = new Properties();
		properties.setProperty("Oracle", "oracle");
		properties.setProperty("MySQL", "mysql");
		databaseIdProvider.setProperties(properties);
		return databaseIdProvider;
	}
}
