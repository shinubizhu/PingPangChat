package com.pingpang.test.service.impl;

import org.apache.dubbo.config.annotation.DubboService;

import com.pingpang.test.service.TestHelloService;

@DubboService
public class TestHelloServiceImpl implements TestHelloService {

	@Override
	public String sayHelloByName(String name) {
		return name+":dubbo";
	}
}
