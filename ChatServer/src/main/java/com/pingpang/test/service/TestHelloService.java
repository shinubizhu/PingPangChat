package com.pingpang.test.service;

public interface TestHelloService {

	String sayHelloByName(String name);
}
