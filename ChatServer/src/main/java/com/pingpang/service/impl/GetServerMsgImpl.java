package com.pingpang.service.impl;

import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.pingpang.service.GetServerMsg;
import com.pingpang.util.IPUtil;

@Service
@DubboService
public class GetServerMsgImpl implements GetServerMsg {
	protected Logger logger = LoggerFactory.getLogger(GetServerMsgImpl.class);

	@Value("${netty.ip}")
	private String nettyIP;
	
	@Value("${netty.port}")
	private String neetyPort;
	
	@Override
	public String getServerIPMsg() {
		if(null==nettyIP || "".equalsIgnoreCase(nettyIP.trim())) {
			nettyIP=IPUtil.getLocalHostExactAddress().getHostAddress();
			logger.info("\r\n----------\r\nip:{}\r\nport:{}\r\n----------",nettyIP,neetyPort);
		}
		return nettyIP+":"+neetyPort;
	}
}
