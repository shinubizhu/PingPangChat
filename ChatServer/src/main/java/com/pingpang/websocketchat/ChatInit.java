package com.pingpang.websocketchat;

import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.pingpang.config.SpringContextUtil;
import com.pingpang.redis.service.RedisService;
import com.pingpang.service.UserGroupService;
import com.pingpang.service.UserMsgService;
import com.pingpang.service.UserService;
import com.pingpang.websocketchat.send.ChatSend;

@Component
public class ChatInit implements CommandLineRunner {

	//日志操作
    private Logger logger = LoggerFactory.getLogger(ChatInit.class);
    
    @Value("${netty.port}")
    private String nettyPort;
    
    @Value("${opencv.save.path}")
    private String opencvSave;
    
    
    // 用户信息处理
    @DubboReference
    public  UserService userService;

 	// 消息信息处理
    @DubboReference
    public  UserMsgService userMsgService;

 	// redis操作
    @DubboReference
    public  RedisService redisService;

 	//群聊信息的获取
    @DubboReference
    public  UserGroupService userGroupService;
    
    @Override
    public void run(String... args) throws Exception {
    	
    	ChatSend.PATH=opencvSave;
    	logger.info("图片保存地址:{}",opencvSave);
        logger.info("The netty start to initialize ...");
        new WebsocketChatServer(Integer.valueOf(nettyPort)).run();
    }
}

