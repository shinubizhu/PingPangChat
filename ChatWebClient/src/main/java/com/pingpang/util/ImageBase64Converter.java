package com.pingpang.util;

import java.awt.Image;
import java.awt.image.BufferedImage;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Base64;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;
import net.coobird.thumbnailator.geometry.Positions;

public class ImageBase64Converter {
	
	//日志操作
    private static Logger logger = LoggerFactory.getLogger(ImageBase64Converter.class);
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
//    	System.out.println(ImageBase64Converter.convertFileToBase64("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\79a3c5734d0f47eeaa0ec9fe8b1c1c56.jpeg"));
//    	System.out.println(ImageBase64Converter.convertFileToBase64Img("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\79a3c5734d0f47eeaa0ec9fe8b1c1c56.jpeg"));
//    	System.out.println(ImageBase64Converter.convertFileToBase64Nio("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\79a3c5734d0f47eeaa0ec9fe8b1c1c56.jpeg"));
//        String img="/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACWAGADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD36kZgqlmOAOSTS15F8Z/HE2lWyaBp0rR3Fwu6eRDgpH6Z9+B+NIDU8V/GTRPD9xLZ2ccmo3cfDLEQEU+5NcOfj9rBfI0a1Cf3TcHP/oNeT29vcX95Fa20Mk9xKwVI0GSxJ/zya72P4L+MHtBN9mtFYjPltMd3+FMD0rwx8bdG1i4jtNSt5dOuH+6zkNGT9f8A61eoRyJLGskbhkYZVgcgivizUdNvdJu3s9QtZLedfvRyY/pwa9k+CXjWVpR4Yvp3kUIXtGkbJAH8GT6DOPYUAe40UUUgCiiigAooooAK+UPihO9x8RtYaRidkixrnsAP/rV9X18y/GbQ5dM8cTXoB8i/QSKccbgACP5mgDpvgHpFnO+p6rJErXMMghjY/wAI2g8fiTXulfLnww8dr4O1eRLsFtPuyPOI6xnGN304FfQEfjzwxJYfbRrVn5GMljKOKAPP/j5pFodBstYCIt1DcCMtnBZSDke/IFeReCrl7LxxossedwutvHoVI/rXUfFX4hw+LruHT9OXOm2rlxKf+Wr9AR7AZ/Osz4V6LLrPj6w2jMVoWuJT6cEAfmRTA+qgcqD7UtFFIAornNb8d+G/D8nlajqlvFL/AM8943flWRbfF/wZcy+X/a0cZJwC5AFAHdUVXsr611G2W4s5454WGQ6NkGrFABXOeNPCFl4x0N7G5wkqnfBNjJjcd/6fjXR0UAfIHiXwdrXha9eLULRvLDfJcRglGHY+30rnSqbtxRc+uK+27mCKe3dJo0kQqcq65FfGHiBVTVdUVVCqtwwAA4A4pgXtB8M6v4lvUt9Os5JskBpCMIg9Sa+m/Afgi18GaQIVYS3koDXE2Mbj6D2FdBpdvDb6fCkEUca7BwigD9KuUgCvFfix8TZrO6k8P6LM0cir/pNwpwVz/Cvv7/SvXtWu/sGj3t3/AM8YHk/JSa+Mry7e8up7ueRmaV2kdjz3/wDrCmBGCzOW+Yux5bqzGnyx3EKgzxTRKw48xCAa+h/hJ4DsNO0CDWr21jl1C7QODIobylPOBnpXomo6LpurWUlpe2UE0LjBV4waAPkvwv4t1XwjqCXOmSYQODLbE4SUdx7HHevqzw14gtfE2h2+p2h+SUfMueVbuK+XvH3hkeFPF13p0ZBtyBLBzyEPb8DmvRPgBqsqz6tpLH9z8lwg9Ccg/wDoIoA90qpqWqWWj2Ml5qFzFb28f3nkYAVLd3UVlay3M7BYolLMT2FfK/j/AMeXnjHVZfmKadE5FvCDwVB4Y+pPX8aQHo/iL48W0LywaFYG5ABAnm+VSfYHBIrwu9ne+ubieXAedzIwXoCfSi3t5ry5S3t4pJp3+5FGu5m+gFdWnww8XvbeeNEm2kZ2lsN+WKYHoHhz47oqw2+uad5SABTNb/MB+HJr2DR9a07XrBL3TbuK4gcZDI2cex9DXxvd2V3p9y1teW01tOoy0cqlT+tbfhLxnqnhDUPtNgyvGQRJbufkf/A0AfU3ih7ZfDGpLdXEUEb20i75GCgZU9zXxuyLtMeflOVyPqa6DxH4v1vxdfB9QuSwdsRW0QIUZ6ADqTTr7wL4j07QV1q80ySKzP3sg70Hqw7CgD6A+FPim01zwja2vnRi+tIxHNFnBGB1x6V3M00VvC8s0ipGgyzMcACvi2yv7vTp1ubK6lt5B0kibGf6GtG/8W+INVtjbX2s3c8LdYywAP1wAaANj4n+I7fxJ42urq0YNbQotvG46Pjkke2SR+Fdn8AdPlbVNW1AoREiRwhvVvmJ/mK8p0fR77W9TisNPgaa4dgAqjO3PdvQV9W+CPCsHhDw5Dp0WWkPzzOerOetAHKfHDWJdP8ABsdnDw99OIic4wuCT/IV83ohd1SNckkKoHqTgD9RXu37QMchsNFkH3BcMD9dteI6awTVLF2OEW5iZs+m8UAfTnw18DWnhbQIJpUWTUrhfMmlK8jPRR9BgV3VQ2jrJZwuhBRkUgj0xU1IDlfHPgyx8W6JLDLGq3UYLQTAcq2K+TriCS1uZbeYASwyNFIB2ZTg/qK+2XIVCT0Aya+OvFs0Vx4y1yaBcRPeybSO+GIP60wPUvgLZaZcDUZJrWOS/glGyZhyFIBAH5mvc3jSSMo6hkIwQRwa8D/Z+hl/trWZcHy9ir+OBXv1IDzzXPg14Y1e5a5hjlsZ2JLNbkAHPtWRB8BNDSQGfU72ZM5KZAz7V61RQBi+H/Cuj+GLXyNKs0hB5ZgOWPua2qKKAOF+LPht/EXgqZYIy91auJ4gDzkAg/oa+Wj64K57dwf8RX28yh1KsAQeCDXz/wDE/wCFs2nXFxreh28k1pK5kmt413NGxPJUDtmmBr/DH4sWUenW+ieIboQzRDZDcyDCuvYMegI6c166ur6a8ImXULUxkZ3CVcfzr4u2nLDGcHDD0+tPFxKE2CSQL6eY2P50AfQXxD+LOnWWnzaZoV0l1fSja0sZykQ9c9M1885LHklj1JPJJ7k+9GAgGcKDwM969L+H3wpufEzpfavFPbaXtJAI2PL6Yz0HegCb4R+O9H8KfaLLVVeI3cwb7VglRwAAcdBx1r3PUfF2h6Zoh1i41GD7FjKyI27d7ADkmvCfGXwd1XQTNeaR5moWC5YpjMkYA9vvflXmhmm8lYDLIYlJKxliVU9+PWgD2HXPj1fSTlNF0+OKEE4luOSR9O34isiD45eKo5Q0q2My55TyyMj61y/hPwNrXjC4ddOiCwxnElxKDsU+nua6nVfgh4j0+wkube4tr1kGTDGhViPbk0AemeC/i3pHieVLK7H2C/Y4WOQ/K5/2W6fhmvRRzzXxJ++tLkq2+GaF8EdGjYfyIr6f+FHi6TxT4XxdlTe2b+VLj+LgEH/PpSA7ykIDDBGQaWigDjvEXw08MeIWknurBY7gqf30XytXyxq0C2N9fQRklYJmjUt1wK+tfEfjbQPDUTDUr9EkI4iT5mP4CvkrWJ0vb+/nhyUmmZ0z1waYH074Y+F3hfRY4LqKy8+42g+bPhjmu6VVRQqgADgAVyfhbx94c8QQRwWOoJ56qAYpBtb9etdbSAyvEsrweGNUljzvW0lIx67TXxo8jeU0n8QDN+OTX2zeW63dlPbP92WNkP0IxXx1r2k3Gg67d6bcLiSCQ446qTkH+f5UwPqL4c6dbad4F0uK2AKtCrsw/iJHWuqr59+GXxWtfD+nLouueYtpFxb3CjdtH91h1r0DVvjL4S0+yaW3vGvJyuUihQ5J+pwBSA8k+M1hbWXxBnNuoXz7dJZAP73TP5AVt/AKV18RarEM7HgjY/XLV5v4j1268S6/dardE+ZOwwpOQigYA/SvaPgP4eltdMv9bnjZDduI4d3dFHX82NMD2KvPfin49bwlpK2tiQdTu/lj/wCma92P+epFehV8r/FnUJr/AOImpLI+UttsMa+gwCf1FIDj7m6uL+8e5uJZJ7iRstI5LMSfT/AVfXwvr72hu10S/aDGd/lHp9Ov6V6H8EfCllrGo3erXsazLZOEhRum/AOT69f0r6EEaBdoVQvpjimB8TI8lrdLKheKeJshhlXUj9RX0Z8JviC/iW1bSNSl3ajbICrkY81B3+vTNcx8cvCNjYQ2/iKzgEUkkvk3KoPlYEEhsevH61558PtQk0vx7o86OV3TGJ/dSp4/PFAH1zXA/ET4b2/jCAXdrsg1SJdqyHgSD+63+e9d8DkA0UgPjnVvCmvaFcGHUNLuoiP40jMiN9CuazYrG5kYrBZ3LN/djtnyf0r7UkgimGJY0cejKDUUenWUTbo7SBT6iMCmB86eC/g9quvTQ3WtQSWOnbgzI7ASSr6YHIB6djX0bZWdvp9nFaWsaxwxKFRVHAFTgY6UUgCvln4t6bNp/wAQ9Qd1xHdbZo29RgA/qa+pq4X4meBF8YaMJLYIupWvzQs38Q7qfqM0AeW/BjxlZaDqtxpN+wiiv3DRyk4AfAG0/kPzr6JWWNkDh1KnnOa+LNR065028ls72BoZozh45Bj/APXUyazqqW/2dNRvhDjGwXDBcfSmB6r8b/GNnqS22gWMizCKXzbl1bgYBAX9T+VcH8O9Nk1Xx9pEKIWCSmZyB0UKefzxXN21rNdTpb2sLSyv92OMZJNfSHwm8AS+GNOOpanGi6pdJyg58lDztz69M+9AHpY4AFFFFIAooooAKKKKACjFFFAGLrvhLQ/EcZXVdNguDjAdl+YfQ9a48/A/wiX3CO6C/wB0XD4/9CoooA6rQPBPh7w2AdM0yCKTGPNK7nP/AAI810GKKKADFGKKKADFGKKKADFFFFAH/9k=";
//        String path="C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\";
//        //ImageBase64Converter.convertBase64ToFile(img, path, UUID.randomUUID()+".jpeg");
//        ImageBase64Converter.convertBase64ToFileImg(img, path, UUID.randomUUID()+"123.jpeg");
        
//        Thumbnails.of(new File("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\d4cac3cf275f4c08ab9b8080288907d0.jpg"))
//        .scale(1f) //图片大小（长宽）压缩比例 从0-1，1表示原图
//        .outputQuality(0.5f) //图片质量压缩比例 从0-1，越接近1质量越好
//        .toOutputStream(new FileOutputStream("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\1.jpg"));
        
        FileUtils.copyInputStreamToFile(
        		ImageBase64Converter.getInputStream(
        				new FileInputStream(
        						new File("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\d4cac3cf275f4c08ab9b8080288907d0.jpg")),
        				0.7f, 0.5f),
        		new File("C:\\Users\\dell\\git\\PingPangChat2\\ChatWebClient\\static\\upload\\1.jpg"));
    }
    
    /**
     * Nio
     * @param imgPath     
     */
	public static String convertFileToBase64Nio(String imgPath) {
		try {
			long currentTime = System.currentTimeMillis();
			String imageType = imgPath.substring(imgPath.lastIndexOf(".") + 1);
			FileInputStream in = new FileInputStream(imgPath);
			logger.info("路径:"+imgPath);
        	logger.info("文件大小（字节）="+in.available());
        	ByteArrayOutputStream out =new ByteArrayOutputStream();
			FileChannel inChannel = in.getChannel();
			ByteBuffer buffer = ByteBuffer.allocate(1024);
			int length=-1;
			while ((length=inChannel.read(buffer)) != -1) {
				// 数据组合.
				out.write(buffer.array(),0,length);
				buffer.clear();
			}
			inChannel.close();
			in.close();
			String base64Str = Base64.getEncoder().encodeToString(out.toByteArray());
			out.close();
			logger.info("耗时:"+String.valueOf(System.currentTimeMillis()-currentTime));
			return "data:image/" + imageType + ";base64," + base64Str;
		} catch (Exception e) {
			logger.error("图片转64失败",e);
		}
		return "";
	}
    
    /**
     * ImageIO
     *
     * @param imgPath     
     */
    public static String convertFileToBase64Img(String imgPath) {
    	long currentTime=System.currentTimeMillis();
    	byte[] data = null;
        String imageType="";
        // 读取图片字节数组
        try {
        	imageType=imgPath.substring(imgPath.lastIndexOf(".")+1);
        	FileInputStream  in = new FileInputStream(imgPath);
        	logger.info("路径:"+imgPath);
        	logger.info("文件大小（字节）="+in.available());
        	ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ImageIO.write(ImageIO.read(in), imageType, stream);
            data=stream.toByteArray(); 	
            stream.close();
            in.close();
        } catch (IOException e) {
        	logger.error("图片转64失败",e);
        }
        String base64Str = Base64.getEncoder().encodeToString(data);
        //logger.info(base64Str);
        logger.info("耗时:"+String.valueOf(System.currentTimeMillis()-currentTime));
        return "data:image/"+imageType+";base64,"+base64Str;
    }
    
    /**
     * ImageIO
     */
    public static void convertBase64ToFileImg(String fileBase64String, String filePath, String fileName) {
    	try {
    		long currentTime=System.currentTimeMillis();
    		File dir = new File(filePath);
            if (!dir.exists() && dir.isDirectory()) {//判断文件目录是否存在
                dir.mkdirs();
            }
            byte[] bfile =Base64.getDecoder().decode(fileBase64String);
            ByteArrayInputStream inputStream=new ByteArrayInputStream(bfile);
            BufferedImage bi=ImageIO.read(inputStream);
            ImageIO.write(bi, fileName.substring(fileName.indexOf(".")+1), new File(filePath + File.separator + fileName));
    	    inputStream.close();
    	    logger.info("耗时:"+String.valueOf(System.currentTimeMillis()-currentTime)); 
    	}catch (Exception e) {
    		logger.error("保存图片失败",e);
		}
    }
    
    /**
     * FileInputStream
     *
     * @param imgPath     
     */
    public static String convertFileToBase64(String imgPath) {
    	long currentTime=System.currentTimeMillis();
        byte[] data = null;
        String imageType="";
        // 读取图片字节数组
        try {
        	imageType=imgPath.substring(imgPath.lastIndexOf(".")+1);
            InputStream in = new FileInputStream(imgPath);
            logger.info("路径:"+imgPath);
            logger.info("文件大小（字节）="+in.available());
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
        	logger.error("图片转64失败",e);
        }
        String base64Str = Base64.getEncoder().encodeToString(data);
        //logger.info(base64Str);
        logger.info("耗时:"+String.valueOf(System.currentTimeMillis()-currentTime));
        return "data:image/"+imageType+";base64,"+base64Str;
    }

    /**
     * FileInputStream
     */
    public static void convertBase64ToFile(String fileBase64String, String filePath, String fileName) {
    	long currentTime=System.currentTimeMillis();
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            File dir = new File(filePath);
            if (!dir.exists() && dir.isDirectory()) {//判断文件目录是否存在
                dir.mkdirs();
            }
            byte[] bfile =Base64.getDecoder().decode(fileBase64String);
            file = new File(filePath + File.separator + fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
        	logger.error("保存图片失败",e);
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                	logger.error("保存图片失败",e1);
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    logger.error("保存图片失败",e1);
                }
            }
            logger.info("耗时:"+String.valueOf(System.currentTimeMillis()-currentTime)); 
        }
    }
    
    public static boolean isImage(String srcFileName) {
		FileInputStream imgFile = null;
		try {
			imgFile = new FileInputStream(srcFileName);
			return isImage(imgFile);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return false;
		}
	}
    
    public static boolean isImage(InputStream inputStream) {
    	try {
    	    Image image = ImageIO.read(inputStream);
    	    return image != null;
    	} catch(IOException ex) {
    	    return false;
    	}
	}
    
    /**
     * 判断文件大小
     *
     * @param len
     *            文件长度
     * @param size
     *            限制大小
     * @param unit
     *            限制单位（B,K,M,G）
     * @return
     */
    public static boolean checkFileSize(Long len, int size, String unit) {
//        long len = file.length();
        double fileSize = 0;
        if ("B".equals(unit.toUpperCase())) {
            fileSize = (double) len;
        } else if ("K".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1024;
        } else if ("M".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1048576;
        } else if ("G".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1073741824;
        }
        if (fileSize > size) {
            return false;
        }
        return true;
    }
    
    
    /**
     * 压缩图像
     * @param image
     * @return
     * @throws IOException
     */
    public static BufferedImage compress(BufferedImage image,float scale,float quality) throws IOException {
        Thumbnails.Builder<BufferedImage> imageBuilder= Thumbnails.of(image).outputQuality(quality);
        return imageBuilder.scale(scale).asBufferedImage();
    }
    
    public static InputStream getInputStream(BufferedImage image, String readImageFormat) throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, readImageFormat, os);
        InputStream is = new ByteArrayInputStream(os.toByteArray());
        os.close();
        return is;
    }
    
    /**
     * 压缩图像
     * @param image
     * @return
     * @throws IOException
     */
    public static InputStream getInputStream(InputStream image,float scale,float quality) throws IOException {
        Builder<? extends InputStream> imageBuilder= Thumbnails.of(image).scale(scale).outputQuality(quality);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(); //字节输出流（写入到内存）
        imageBuilder.toOutputStream(baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }
    
    public static InputStream getInputStream(MultipartFile file) throws IOException {
    	//是图片格式
        InputStream in=null;
        logger.info("文件大小（字节）="+file.getSize());
        if(isImage(file.getInputStream())) {
        	logger.info("图片格式...");
        	//M
        	double fileSize = file.getSize()/1048576;
        	logger.info(fileSize+"M");
        	if(fileSize>1 && fileSize<5) {
        		//in=getInputStream(compress(ImageIO.read(file.getInputStream()), 1.0f, 0.6f),FilenameUtils.getExtension(file.getOriginalFilename()));
        		in=getInputStream(file.getInputStream(), 1.0f, 0.6f);
        	}else if(fileSize>5) {
        		//in=getInputStream(compress(ImageIO.read(file.getInputStream()), 0.7f, 0.5f),FilenameUtils.getExtension(file.getOriginalFilename()));
        		in=getInputStream(file.getInputStream(), 0.7f, 0.5f);
        	}
        	if(null!=in) {
        		logger.info("转换后大小（字节）="+in.available());
        	}
        }else {
        	logger.info("非图片格式...");
        }
        return in;
    }

}
