package com.pingpang.minio;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import io.minio.BucketExistsArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.MinioException;
import io.minio.http.Method;

public class MinioTest {

	public static void main(String[] args) {
		try {
			// 使用MinIO服务的URL，端口，Access key和Secret key创建一个MinioClient对象
//			MinioClient minioClient = MinioClient.builder().endpoint("http://localhost:9000")
//					.credentials("minioadmin", "minioadmin").build();
			
			MinioClient minioClient = MinioClient.builder().endpoint("https://192.168.1.100:9000",9000,true)
					.credentials("minioadmin", "minioadmin").build();

			// 检查存储桶是否已经存在
			String bucketName = "image";
			String fileName = UUID.randomUUID().toString() + ".jpg";
			boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
			if (isExist) {
				System.out.println(bucketName + "存在");
			} else {
				// 创建一个名为test的存储桶，用于存储照片的zip文件。
				minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
			}

			// 使用putObject上传一个文件到存储桶中。
			FileInputStream in = new FileInputStream("E:\\minio\\test.jpg");
			minioClient.putObject(
					PutObjectArgs.builder().bucket(bucketName).object(fileName).stream(in, in.available(), -1).build());

			// 查看文件地址
			GetPresignedObjectUrlArgs build = new GetPresignedObjectUrlArgs().builder().bucket(bucketName)
					.object(fileName).method(Method.GET).build();
			String url = minioClient.getPresignedObjectUrl(build);

			System.out.println(url);

		} catch (MinioException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
