package com.pingpang.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import com.pingpang.util.IPUtil;

import eu.bitwalker.useragentutils.UserAgent;
 
public class RequestHandlerInterceptor implements HandlerInterceptor {
	Logger logger = LoggerFactory.getLogger(RequestHandlerInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String userAgent = request.getHeader("user-agent");
		UserAgent ua=UserAgent.parseUserAgentString(userAgent);
		String basePath = request.getContextPath();
		String path = request.getRequestURI();
		String ip=IPUtil.getIpAddr(request);
		logger.info("\r\n--------------\r\nuserAgent:{}\r\nIP:{}\r\nos:{}\r\nbrowser:{}\r\nbasePath:{}\r\npath:{}",userAgent,ip,ua.getOperatingSystem().getName(),ua.getBrowser().getName()+" "+ua.getBrowserVersion(),basePath,path);
		return true;		
	}
}
