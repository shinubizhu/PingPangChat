package com.pingpang.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.pingpang.interceptor.LoginHandlerInterceptor;
import com.pingpang.interceptor.RequestHandlerInterceptor;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	/**
	 * 不需要登录拦截的url
	 */
	final String[] notLoginInterceptPaths = { "/audio/**",
			                                  "/layer/**",
			                                  "/map/**",
			                                  "/util/**",
			                                  "/x-admin/**",
			                                  /* "/druid/**", */
			                                  "/weui/**",
			                                  "/error",
			                                  "/user/chat",
			                                  "/user/index",
			                                  "/",//默认ip+端口 直接进入登录页面
			                                  "/user/regist",
			                                  "/user/addUser",
			                                  "/actuator/**",
			                                  "/autho/rtmp",//权限验证接口
			                                  "/autho/rtmpOver"//移除token
			                                  };

	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new RequestHandlerInterceptor()).addPathPatterns("/**");
		registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns(notLoginInterceptPaths);
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("classpath:/META-INF/resources/");
	}
	
	public void addViewControllers(ViewControllerRegistry registry) {
		// 设置系统访问的默认首页
		registry.addViewController("/").setViewName("index");
		//registry.addViewController("/").setViewName("blog");
	}
	
	/*
	 * @Override public void addViewControllers(ViewControllerRegistry registry) {
	 * registry.addViewController("/").setViewName("forward:/WEB-INF/index");
	 * registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	 * WebMvcConfigurer.super.addViewControllers(registry); }
	 */
}
