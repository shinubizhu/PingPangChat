<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
<meta charset="utf-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

<link rel="stylesheet" href="${httpServletRequest.getContextPath()}/weui/css/weui.css" />
<link rel="stylesheet" href="${httpServletRequest.getContextPath()}/weui/css/weuix.css" />
<link rel="stylesheet" href="${httpServletRequest.getContextPath()}/weui/css/chat.css" />
<script src="${httpServletRequest.getContextPath()}/weui/js/zepto.min.js"></script>
<script src="${httpServletRequest.getContextPath()}/weui/js/zepto.weui.js"></script>
<script src="${httpServletRequest.getContextPath()}/weui/js/lrz.min.js"></script>
<script src="${httpServletRequest.getContextPath()}/weui/js/xss.min.js"></script>
<script src="${httpServletRequest.getContextPath()}/util/dateFormat.js"></script>
<script src="${httpServletRequest.getContextPath()}/util/msgUtil.js"></script>
<script src="${httpServletRequest.getContextPath()}/audio/HZRecorder.js"></script>
<script src="${httpServletRequest.getContextPath()}/audio/voiceEncoder.js"></script>
<script src="${httpServletRequest.getContextPath()}/audio/queue.js"></script>
<script src="${httpServletRequest.getContextPath()}/util/voiceUtil.js"></script>
<script type="text/javascript">
var path="${httpServletRequest.getContextPath()}";
var currentPage=1;
var limit=5;
    
$(function(){
 	//防止页面后退
  history.pushState(null, null, document.URL);
  window.addEventListener('popstate', function () {
      history.pushState(null, null, document.URL);
  });
   
$('#tb').tab({
  defaultIndex: 0,
  activeClass: 'weui-bar__item_on',
  onToggle: function (index) {
      //$("#chat"+index).show();
  }
});
      
$('.single-bottom').on('click','.s-btn',function(){
  if(canSend==false) return;
  if(null==$("#chatMsg").val()||""==$("#chatMsg").val().trim()){
	return;
  }
  sendMsg($("#userChatCode").html(),3,$("#chatMsg").val());
  reply(userCode,userName,$("#chatMsg").val(),'',(new Date()).Format("yyyy-MM-dd hh:mm:ss"),1);
  $("#chatMsg").val('');
  scrollToBottom();
})


$('.group-bottom').on('click','.g-btn',function(){
	if(canSend==false) return;
	if(null==$("#groupMsg").val()||""==$("#groupMsg").val().trim()){
		return;
	}
	sendMsg($("#groupChatCode").html(),4,$("#groupMsg").val(),true);
	reply(userCode,userName,$("#groupMsg").val(),'',(new Date()).Format("yyyy-MM-dd hh:mm:ss"),1,"4");
	$("#groupMsg").val('');
	scrollToBottom();
})

$("#chatMsg").blur(function(){
	setSendBtn();
});
      
$("#groupMsg").blur(function(){
      	setGroupSendBtn();
});
 //reply(11,'系统','你好工工工工工','','1月27日 11:33',0);
 scrollToBottom();

//底部切换
$('.weui-tabbar__item').on('click', function () {
          $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
});

 navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
});
    
function setSendBtn(){
	var pbtn = $(".s-btn");
	if($("#chatMsg").val().length>0){
		pbtn.css('background','#114F8E').prop('disabled',true);
		canSend=true;
	}else{
		canSend=false;
		pbtn.css('background','#ddd').prop('disabled',false);
	}
}
  
function setGroupSendBtn(){
		var pbtn = $(".g-btn");
		if($("#groupMsg").val().length>0){
			pbtn.css('background','#114F8E').prop('disabled',true);
			canSend=true;
		}else{
			canSend=false;
			pbtn.css('background','#ddd').prop('disabled',false);
		}
}
   
    var timSendStart;//推送定时器
    var timSendEnd;//推送定时器
  //用户编号
    var userCode="${userCode}";
    var userName="${userName}";
    var tryLink=0;//尝试连接服务器次数
    var ws;
    var WebSocketFunc = {};
    WebSocketFunc.init = function(uri,token) {//ws开始
    ws = new WebSocket("wss://"+uri+"/ws");
    //ws = new WebSocket("ws://"+"192.168.0.103:8086"+"/ws");
    ws.onopen = function(){  
       //console.log("open");
       //初始化绑定信息
       var bindMsg = {};
       var from={};
       
       bindMsg.cmd="1";
       from.userCode=userCode;
       from.userName=userName;
       bindMsg.from=from;
       bindMsg.msg=token;
       //console.log("登录");
       ws.send(JSON.stringify(bindMsg));
    };

    ws.onmessage = function(evt){
      //console.log(evt.data);
      if("pong"==evt.data){
    	  return;
      }else if("Heartbeat"==evt.data){
       ws.send("Heartbeat");
    	return;  
      }
      //console.log(eval("("+evt.data+")"));
      var message=eval("("+evt.data+")");
      if("-1"==message.cmd){//退出
    	  $.toast(message.msg, 'text');
    	  window.location.href="${httpServletRequest.getContextPath()}/user/logOut";
    	  return false;
       }else if("1"==message.cmd){//登录成功
       	sendMsg("",10,"");//获取最近聊天人员
       	getAllUser(5,"");//获取在线人员
    	getAllUser(6,""); //获取群组信息
       }else if("2"==message.cmd){//广播
       	if(message.from.userCode==$("#userChatCode").html()){
       		reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,0);
       		scrollToBottom();
       	}
       
       	if($("#chatNewMsg"+message.from.userCode).length<1){
               $("#chat0").append(getOldUserCodeHmtl(message.from.userCode,message.from.userName));
       	}   
       
       	$("#chatNewMsg"+message.from.userCode).html(message.msg);
       	
       }else if("3"==message.cmd){//获取单聊
       	var isImage=false;
       	if(message.from.userCode==$("#userChatCode").html()){
       		var base64Img=loadImage(message.msg);
       		if(""!=base64Img){
       			message.msg=base64Img;
       			isImage=true;
       		}
       		reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,0);
       		scrollToBottom();
       	}
       	
       	if(isImage || message.msg.startsWith('<img src="')){
       		message.msg="有图片过来...";
       	}
       
       	if($("#chatNewMsg"+message.from.userCode).length<1){
               $("#oldUser").append(getOldUserCodeHmtl(message.from.userCode,message.from.userName));
       	}   
       	
       	$("#chatNewMsg"+message.from.userCode).html(message.msg);
       	
       }else if("4"==message.cmd){//群聊信息
       	var isImage=false;
       	if(message.group.groupCode==$("#groupChatCode").html()){
       		var base64Img=loadImage(message.msg);
       		if(""!=base64Img){
       			message.msg=base64Img;
       			isImage=true;
       		}
       		reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,0,"4");
       		scrollToBottom();
       	}
       	if(isImage || message.msg.startsWith('<img src="')){
       		message.msg="有图片过来...";
       	}
       	$("#grop_"+message.group.groupCode).html(message.msg);

       }else if("5"==message.cmd){//获取在线的用户信息
     	  var currentUser=message.chatSet;
   	  $("#onlineUser").html("");
   	  for(var i = 0; i < currentUser.length; i++){
            $("#onlineUser").append(viewOnlineUser(currentUser[i].userCode,currentUser[i].userName,3));
   	  }
       }else if("6"==message.cmd){//获取在线群组信息
       	  var currentUser=message.groupSet;
       	  $("#onlineGroup").html("");
       	  var groupShow='<ul class="collapse">';
       	  for(var i = 0; i < currentUser.length; i++){
       		  //console.log(currentUser[i]);
       		  groupShow=groupShow+viewOnlineUser(currentUser[i].groupCode,currentUser[i].groupName,4);
       	  }
       	  groupShow=groupShow+'</ul>';
       	  $("#onlineGroup").append(groupShow);
       }else if("9"==message.cmd){//获取历史聊天信息
       	//console.log(message);
       	var currentUser=message.oldMsg;
     	    for(var message of currentUser){//单聊
     	    	var base64Img=loadImage(message.msg);
       		if(""!=base64Img){
       			message.msg=base64Img;
       		}
     		  if("3"==message.cmd){
     			if(userCode!=message.from.userCode){
     				reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,0,"3",false);
     				//scrollToBottom();
     			}else{
     				reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,1,"3",false);
     				//scrollToBottom();
     			}
     		  }else if("4"==message.cmd){//群聊
     			if(userCode!=message.from.userCode){
     				reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,0,"4",false);
     				//scrollToBottom();
     			}else{
     				reply(message.from.userCode,message.from.userName,message.msg,'',message.createDate,1,"4",false);
     				//scrollToBottom();
     			}
     		  }
     	    }
       }else if("10"==message.cmd){//获取历史聊天用户
     	  var currentUser=message.chatSet;
   	  $("#oldUser").html("");
   	  for(var i = 0; i < currentUser.length; i++){
   		  if(null==currentUser[i].userCode){
   			  continue;
   		  }
             $("#oldUser").append(getOldUserCodeHmtl(currentUser[i].userCode,currentUser[i].userName));
   	  }
       }else if("14"==message.cmd){//request refuse accept
     	  if("accept"==message.msg){
     		star();
     		timSendEnd=setInterval(sendVoice ,800,message.from.userCode);
     		 $.modal({
                title: "语音中",
                text: userCode+"~~~~"+message.from.userCode,
                buttons: [
                    { text: "取消", className: "default", onClick: function(){ 
                   	  sendMsg(message.from.userCode,14,"close");
                   	  stop();
       		          clearInterval(timSendStart);
    		              clearInterval(timSendEnd);} },
                ]
            });
   	  }else if("request"==message.msg){
   		  $.confirm("是否接收语音通话?", "", function() {
   	    		 sendMsg(message.from.userCode,14,"accept");
   	    		 
   	    		 $.modal({
   	                 title: "语音中",
   	                 text: userCode+"~~~~"+message.from.userCode,
   	                 buttons: [
   	                     { text: "取消", className: "default", onClick: function(){ 
   	                    	 sendMsg(message.from.userCode,14,"close");
   	                    	 stop();
   	        		         clearInterval(timSendStart);
   	     		             clearInterval(timSendEnd);	 
   	                     } },
   	                 ]
   	             });
   	    		 
   	    		 star();
   	    		 timSendStart=setInterval(sendVoice ,800,message.from.userCode);
   	    		 
   	         }, function() {
   	        	 sendMsg(message.from.userCode,14,"refuse");
   	         });
   	  }else if("refuse"==message.msg){
   		  $.toast('对方拒绝。。。', "cancel");
   	  }else if("close"==message.msg){
   		  stop();
   		  clearInterval(timSendStart);
   		  clearInterval(timSendEnd);
   		  $.closeModal();
   		  $.toast('聊天结束。。。','text');
   	  }else{//接收的语音信息
   		  remotePlay(message.msg);
   	  }
     }
    };
    
    ws.onerror = function(evt){
   	  stop();
   	  clearInterval(timSendStart);
	  clearInterval(timSendEnd);
   	  console.log("WebSocketError!");
    };
   
    ws.onclose = function(evt){
   	 stop();
   	 clearInterval(timSendStart);
	 clearInterval(timSendEnd);
      //这里三次重试机会
     for(;tryLink<5;tryLink++){
   	  $.toast('连接中断获取服务地址['+(tryLink+1)+']次!', 1000); 
    	  sleep(2000); //当前方法暂停1秒
    	  var tryLogin=false;
    	  $.ajax({
    		    url:"${httpServletRequest.getContextPath()}/user/server-list",
    		    type:"Post",
    		    async:false,//同步
    		    dataType:"json",
    		    success:function(data){
    		    	var nettyServer=data.nettyServer;
    		    	//console.log("获取server:"+nettyServer);
    		    	if(null!=nettyServer && ""!=nettyServer){
    		    		 WebSocketFunc.init(nettyServer,data.loginToken);
    		    		 tryLogin=true;
    		    		console.log("获取成功。。。");
    		    	}else{
    		    		console.log("获取失败。。。");
    		    		nettyServer=false;
    		    	}
    		    },
    		     error:function(data){
    		     console.log("获取服务器失败...");
    		     nettyServer=false;
    		    }
    		});
    	  
    	  if(tryLogin){
    		  return;
    	  }
      }
    	
      console.log("WebSocketClosed!");
      clearSchedule();
      $.toast('连接中断...稍后请刷新!', 'text'); 
    };
    }//ws结束
    
    //取消掉定时任务
    function clearSchedule(){
    	  clearInterval(getUserT);
    	  clearInterval(getGroupUserT);
    	  clearInterval(timSendStart);
	      clearInterval(timSendEnd);
    	  //clearInterval(getPingT);
    	  //clearInterval(getOldUserCodeT);
    	  //clearInterval(getAudioLiveUserCodeT);
    }

    //websocket初始化
    WebSocketFunc.init("${nettyServer}","${loginToken}");
    
    var getUserT = setInterval(function(){getAllUser(5,"");},10000);
    var getGroupUserT = setInterval(function(){getAllUser(6,"");},60000);
</script>
</head>
<body>

	<div class="page">
	    <!-- 图片放大 -->
		<div class="weui-gallery" id="weui-gallery" onclick="hideImgShow();"
			style="display: none">
			<span class="weui-gallery__img"></span>
			<div class="weui-gallery__opr"></div>
		</div>

		<div style="display: none">
			<!--   <div> -->
			<!-- 语音聊天 -->
			<select id="audioSource"></select>
			<!--  -->
			<div>
				<audio id="audio_from" type="audio/wav" controls autoplay></audio>
				<audio id="audio_accept" controls autoplay></audio>
			</div>
		</div>
		<div class="page__bd" style="height: 100%;">

			<div id="chatFull" class='weui-popup__container'
				style="z-index: 777;">
				<div class="weui-popup__overlay"></div>
				<div class="weui-popup__modal">
					<div class="weui-header bg-blue"
						style="position: fixed; top: 0; width: 100%; z-index: 500;">
						<div class="weui-header-left">
							<a class="icon icon-109 f-white close-popup"
								href='javascript:closeViewChat("chatFull");'>返回</a>
						</div>
						<h1 class="weui-header-title" id="userChatName"></h1>
						<h1 class="weui-header-title hide" id="userChatCode"></h1>
						<div class="weui-header-right">
							<a class="f-white" href='javascript:onLineVoice();'>语音</a> <a
								class="f-white">视频</a> <a class="icon icon-83 f-white">更多</a>
						</div>
					</div>
					<div class="weui-tab__panel" id="userChat" style="margin:0 0 20px 0;"></div>
					<div class="weui-grids tab-bottom weui-tabbar single-bottom" id="sendmsg">
						<div class="weui-grid" style="width: 62%;">
							<p class="weui-grid__label">
								<input type="text" id="chatMsg" class="weui-form-input"
									style="width: 96%;" cursor-spacing="140" />
							</p>
						</div>
						<div class="weui-grid" style="width: 20%;">
							<p class="weui-grid__label p-btn s-btn">发送</p>
						</div>
						<div class="weui-cells weui-cells_form" id="uploader">
							<div class="weui-uploader__input-box"
								style="width: 45px; height: 45px;">
								<input id="uploaderInput" class="weui-uploader__input"
									accept="image/*" multiple="" type="file"
									onchange="previewImages(this)">
							</div>
						</div>
					</div>
				</div>
			</div>


            <div id="chatGroupFull" class='weui-popup__container'
				style="z-index: 777;">
				<div class="weui-popup__overlay"></div>
				<div class="weui-popup__modal">
					<div class="weui-header bg-blue"
						style="position: fixed; top: 0; width: 100%; z-index: 500;">
						<div class="weui-header-left">
							<a class="icon icon-109 f-white close-popup"
								href='javascript:closeViewChat("chatGroupFull");'>返回</a>
						</div>
						<h1 class="weui-header-title" id="groupChatName"></h1>
						<h1 class="weui-header-title hide" id="groupChatCode"></h1>
						<div class="weui-header-right">
						  <a class="icon icon-83 f-white">更多</a>
						</div>
					</div>
					<div class="weui-tab__panel" id="groupChat" style="margin:0 0 20px 0;"></div>
					<div class="weui-grids tab-bottom weui-tabbar group-bottom" id="sendmsg">
						<div class="weui-grid" style="width: 62%;">
							<p class="weui-grid__label">
								<input type="text" id="groupMsg" class="weui-form-input"
									style="width: 96%;" cursor-spacing="140" />
							</p>
						</div>
						<div class="weui-grid" style="width: 20%;">
							<p class="weui-grid__label p-btn g-btn">发送</p>
						</div>
						<div class="weui-cells weui-cells_form" id="uploader">
							<div class="weui-uploader__input-box"
								style="width: 45px; height: 45px;">
								<input id="uploaderInput" class="weui-uploader__input"
									accept="image/*" multiple="" type="file"
									onchange="previewImages(this)">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id='tb' class="weui-tab">
				<div class="weui-tab__panel">
					<div id="chat0" class="weui-tab__content">
						<!--信息-->
						<div class="weui-cells demo_badge_cells" id="oldUser"></div>
						<div class="weui-cells demo_badge_cells" id="onlineGroup"></div>
					</div>

					<div id="chat1" class="weui-tab__content">
						<!--在线-->
						<div class="weui-cells demo_badge_cells" id="onlineUser"></div>
					</div>
					<div id="chat2" class="weui-tab__content">准备中</div>
					<div id="chat3" class="weui-tab__content">
						<div class="weui-form-preview__bd">
							<div class="weui-form-preview__item">
								<label class="weui-form-preview__label">用户名</label> <span
									class="weui-form-preview__value">${userName}</span>
							</div>
							<div class="weui-form-preview__item">
								<label class="weui-form-preview__label">昵称</label> <span
									class="weui-form-preview__value">${userCode}</span>
							</div>
							<div class="weui-form-preview__ft">
								<a class="weui-form-preview__btn weui-form-preview__btn_primary"
									href="${httpServletRequest.getContextPath()}/user/logOut">退出</a>
							</div>
						</div>
					</div>
				</div>

				<div class="weui-tabbar tab-bottom">
					<div class="weui-tabbar__item weui-bar__item_on">
						<div style="display: inline-block; position: relative;">
							<img
								src="${httpServletRequest.getContextPath()}/weui/images/icon_tabbar.png"
								alt="" class="weui-tabbar__icon"> <span
								class="weui-badge weui-badge_dot"
								style="position: absolute; top: 0; right: -6px;"></span>
						</div>
						<p class="weui-tabbar__label">信息</p>
					</div>
					<div class="weui-tabbar__item">
						<img
							src="${httpServletRequest.getContextPath()}/weui/images/icon_tabbar.png"
							alt="" class="weui-tabbar__icon">
						<p class="weui-tabbar__label">在线</p>
					</div>
					<div class="weui-tabbar__item">
						<div style="display: inline-block; position: relative;">
							<img
								src="${httpServletRequest.getContextPath()}/weui/images/icon_tabbar.png"
								alt="" class="weui-tabbar__icon"> <span
								class="weui-badge weui-badge_dot"
								style="position: absolute; top: 0; right: -6px;"></span>
						</div>
						<p class="weui-tabbar__label">发现</p>
					</div>
					<div class="weui-tabbar__item">
						<img
							src="${httpServletRequest.getContextPath()}/weui/images/icon_tabbar.png"
							alt="" class="weui-tabbar__icon">
						<p class="weui-tabbar__label">我</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>