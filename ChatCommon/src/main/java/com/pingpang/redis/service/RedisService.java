package com.pingpang.redis.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.redis.core.query.SortQuery;

import com.pingpang.websocketchat.Message;

public interface RedisService {

	/**
	 * 添加数据
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, Object value);

	/**
	 * 获取数据
	 * 
	 * @param key
	 * @return
	 */
	public Object get(String key);

	/**
	 * 删除数据
	 * 
	 * @param key
	 * @return
	 */
	public boolean delete(String key);

	/**
	 * 删除数据匹配前缀
	 * 
	 * @param prex
	 * @return
	 */
	public void deletePrex(String prex);

	/**
	 * 获取key值
	 * 
	 * @param prex
	 * @return
	 */
	public Set<String> getSetPrex(String prex);

	/**
	 * 判断key是否存在
	 * 
	 * @param key
	 * @return
	 */
	public boolean isExitKey(String key);

	/**
	 * 统计数量
	 * 
	 * @param prex
	 * @return
	 */
	public int getCountPrex(String prex);

//--------------------set开始-----------------------------------------------------------------------------    
	/**
	 * 获取set数据
	 * 
	 * @param key
	 * @return
	 */
	public Set<Object> getSet(String key);

	/**
	 * 获取set集合的个数
	 * 
	 * @param key
	 * @return
	 */
	public Long getSetCount(String key);

	/**
	 * 单条set添加
	 * 
	 * @param key
	 * @param ojbSet
	 */
	public void addSet(String key, Object objSet);

	/**
	 * 添加set数据
	 * 
	 * @param key
	 * @param ojbSet
	 */
	public void addSet(String key, Set<Object> objSet);

	/**
	 * 判断是否存在
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean contains(String key, Object value);

	/**
	 * 判断是否包含某个元素
	 * 
	 * @param key
	 * @param value
	 */
	public boolean contains(String key, String name, Object value);

	/**
	 * 移除set数据
	 * 
	 * @param key
	 * @param value
	 */
	public void removeSet(String key, Object value);

	/**
	 * 下线删除群组里面的用户信息
	 * 
	 * @param key
	 * @param value
	 */
	public void delGroupUser(String key, Object name, Object value);

//--------------------set结束-----------------------------------------------------------------------------    
//--------------------zset开始----------------------------------------------------------------------------
	public void addZset(String key, String value, double score);

	public void delZset(String key, String... values);

	public Set<String> getAllServer(String key);

	public void increZset(String key, String value, double score);

	/**
	 * 获取最小的值
	 * 
	 * @param key
	 * @return
	 */
	public String getServer(String key, int incryBY);

	/**
	 * 随机值
	 * 
	 * @param key
	 * @return
	 */
	public String getRandomServer(String key, int incryBY);

	/**
	 * 排序操作
	 * 
	 * @param query
	 * @return
	 */
	public List<Object> sort(SortQuery query);

//--------------------zset结束----------------------------------------------------------------------------    
	/**
	 * map类型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void addHashMap(String key, Map<String, Object> value);

	/**
	 * map类型数据添加
	 * 
	 * @param key
	 * @param value
	 */
	public void addHashMap(String key, String mapKey, Object value);

	/**
	 * map数据值自增长1
	 * 
	 * @param mapKey
	 * @param key
	 */
	public void addHashMapVlaue(String mapKey, String key);

	/**
	 * 判断Map是否存在
	 * 
	 * @param key
	 * @param keyMap
	 * @return
	 */
	public boolean isHashMap(String key, String keyMap);

	/**
	 * 删除map的key
	 * 
	 * @param key
	 * @param keyMap
	 * @return
	 */
	public long removeMap(String key, String keyMap);

	/**
	 * 获取map数据
	 * 
	 * @param key
	 * @return
	 */
	public Map<Object, Object> getHashMapKeys(String key);

	/**
	 * 获取hashMap内容
	 * 
	 * @param key
	 * @param mapKey
	 * @return
	 */
	public Object getHshMapValue(String key, String mapKey);

	/**
	 * 发布消息
	 * 
	 * @param msg
	 */
	public void SendMsg(Message msg);

	/**
	 * 为json串
	 * 
	 * @return
	 */
	public List<String> getAllUpUser();

	/**
	 * 添加存活时间
	 * 
	 * @param key
	 * @param secordTime
	 */
	public void expireKey(String key, int timeout);
}
