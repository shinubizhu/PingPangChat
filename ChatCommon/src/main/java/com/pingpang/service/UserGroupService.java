package com.pingpang.service;

import java.util.Map;
import java.util.Set;

import com.pingpang.websocketchat.ChatGroup;

public interface UserGroupService {

	/**
	 * 获取所有群组信息
	 * 
	 * @param queryMap
	 * @return
	 */
	public Set<ChatGroup> getAllGroupSet(Map<String, String> queryMap);

	/**
	 * 获取群组总数
	 * 
	 * @param queryMap
	 * @return
	 */
	public int getAllGroupCount(Map<String, String> queryMap);

	/**
	 * 添加群组
	 * 
	 * @param cg
	 */
	public Map<String, Object> addGroup(ChatGroup cg);

	/**
	 * 跟新群组信息
	 * 
	 * @param cg
	 */
	public void updateGroup(ChatGroup cg);

	/**
	 * 获取群组信息
	 * 
	 * @param queryMap
	 * @return
	 */
	public ChatGroup getGroup(Map<String, String> queryMap);

	/**
	 * 批量更新数据
	 * 
	 * @param status
	 * @param ids
	 */
	public void updateGroupS(String status, Set<String> ids);

}
